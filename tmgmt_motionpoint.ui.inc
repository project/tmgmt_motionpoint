<?php

/**
 * @file
 * Provides MotionPoint translation plugin controller.
 */
class TMGMTMotionpointTranslatorUIController extends TMGMTDefaultTranslatorUIController {
    
  /**
   * Constants
   */
  const MP_JOB_STATUS_QUEUED = 'QUEUED';
  const MP_JOB_STATUS_COMPLETED = 'COMPLETED';
  const MP_JOB_STATUS_APPROVED = 'APPROVED';

  
  /**
   * {@inheritdoc}
   */
  public function pluginSettingsForm($form, &$form_state, TMGMTTranslator $translator, $busy = FALSE) {
    $api_base_url = filter_var($translator->getSetting('motionpoint_api_url'), FILTER_VALIDATE_URL) 
                   ? $translator->getSetting('motionpoint_api_url') 
                   : 'https://api.motionpoint.com';

    $form['motionpoint_api_url'] = [
      '#type' => 'textfield',
      '#title' => t('MotionPoint API URL'),
      '#default_value' => $api_base_url,
      '#description' => t('Please enter MotionPoint API base URL.'),
      '#required' => TRUE,
    ];
    $form['motionpoint_username'] = [
      '#type' => 'textfield',
      '#title' => t('Your Username'),
      '#description' => t('Please enter username'),
      '#required' => TRUE,
    ];
    $form['motionpoint_apikey'] = [
      '#type' => 'textfield',
      '#title' => t('API Key'),
      '#description' => t('Please enter API-Key'),
      '#required' => TRUE,
      '#maxlength' => 256,
    ];
    $form['motionpoint_client_id'] = [
      '#type' => 'textfield',
      '#title' => t('MotionPoint API ID'),
      '#description' => t('Please enter MotionPoint API ID'),
      '#required' => TRUE,
    ];
    $form['use_sandbox'] = [
      '#type' => 'checkbox',
      '#title' => t('Use Sandbox'),
      '#default_value' => $translator->getSetting('use_sandbox'),
      '#description' => t('Check to use MotionPoint Sandbox environment.'),
      '#required' => FALSE,
    ];
    $form['submit_setting'] = [
      '#type' => 'radios',
      '#title' => t('Default Submit Setting'),
      '#required' => TRUE,
      '#default_value' => $translator->getSetting('submit_setting'),
      '#description' => t('Default Submit Setting of the project selected in job checkout.'),
      '#options' => $this->getSubmitSetting(),
    ];
    
    $form['connect'] = [
      '#type' => 'submit',
      '#value' => t('Connect'),
      '#submit' => array('tmgmt_motionpoint_controller_connect_submit'),
        '#limit_validation_errors' => array(
        array(
          'settings',
          'motionpoint_username'
        ),
        array(
          'settings',
          'motionpoint_apikey'
        ),
        array(
          'settings',
          'motionpoint_api_url'
        ),
        array(
          'settings',
          'use_sandbox'
        ),
      ),
    ];
    
    return parent::pluginSettingsForm($form, $form_state, $translator, $busy);
  }
  
  
  /**
   * {@inheritdoc}
   */
  public function checkoutSettingsForm($form, &$form_state, TMGMTJob $job) {
    $controller = $job->getTranslatorController();

    try {
     $quote = $controller->getStatistics($job);
    }
    catch (Exception $e) {
      watchdog('tmgmt_motionpoint', $e->getMessage(), array(), WATCHDOG_ERROR);
    }

    $settings['quote'] = array(
        '#type' => 'fieldset',
        '#title' => t('Translation Statistics'),
    );
    
    if (!$quote) {
      $settings['quote']['error'] = array(
        '#type' => 'item',
        '#markup' => t('Statistics cannot be calculated. Please try again in a few minutes. If situation persists please contact support@motionpoint.com.'),
      );
      
      return $settings;
    }

    if (array_key_exists('language_not_support', $quote)) {
      $settings['quote']['language_not_support'] = array(
        '#type' => 'item',
        '#markup' => t('The target language is not supported. Please contact support@motionpoint.com.'),
      );  
      return $settings;
    }
    
    $settings['quote']['words_not_translated'] = array(
      '#type' => 'item',
      '#title' => t('Untranslated words'),
      '#markup' => $quote['words_not_translated']
    );

    $settings['quote']['words_total'] = array(
      '#type' => 'item',
      '#title' => t('Total words'),
      '#markup' => $quote['words_total'],
      '#description' => t('<br>Please note that MotionPoint provides the unique translatable word count, which may differ from the Drupal word count.'),
    );

    $translator = $job->getTranslator();
    $mode_from_translator = $translator->getSetting('submit_setting');
    $settings['submit_setting' . strval($mode_from_translator)] = [
      '#type' => 'radios',
      '#title' => t('Submit Setting'),
      '#default_value' => !is_null($mode_from_translator) ? $mode_from_translator : 0,
      '#options' => $this->getSubmitSetting(),
    ];
    
    return $settings;
  }

  
  /**
   * {@inheritdoc}
   */
  public function checkoutInfo(TMGMTJob $job) {
    $form = [];

    $controller = $job->getTranslatorController();
    // Need to set translator here or an exception 
    // will occur in $controller->sendRequest
    $translator = $job->getTranslator();
    $controller->setTranslator($translator);
    
    $source_language = $translator->mapToRemoteLanguage($job->source_language);
    $target_language = $translator->mapToRemoteLanguage($job->target_language);
    // Job reference tracks id and remote status
    $job_reference = $job->reference;
    $api_job_id = explode("-", $job_reference, 2)[0];
    $pulled_translation = FALSE;
    
    $mode_from_translator = $translator->getSetting('submit_setting');
    $mode_from_job = $job->getSetting('submit_setting' . strval($mode_from_translator));

    if (!is_null($mode_from_job) && $mode_from_job == 1) {
      if ($job->isActive() && $job->getCountPending() > 0) {
        $form['motionpoint_status'] = [
          '#markup' => t("This job is submitted in Multiple Jobs mode. Please click button below to pull already translated item.<br><br>"),
          '#weight' => -10,
        ];
        $form['actions']['pull'] = [
          '#type' => 'submit',
          '#value' => t('Pull translations'),
          '#submit' => [[$this, 'submitPullTranslations']],
          '#weight' => -10,
        ];
      }
    }
    else {
      if ($job->isActive() && $job->getCountPending() > 0) {
        $api_job_status = $controller->getApiJobStatus($api_job_id, $source_language, $target_language);
        
        if ($api_job_status == static::MP_JOB_STATUS_COMPLETED) {
          $job->reference = $job_reference . '-' . static::MP_JOB_STATUS_COMPLETED;
          $job->save();
          $pulled_translation = $controller->updateTranslationsToJob($job, $api_job_id);
          
          if ($pulled_translation) {
            drupal_set_message(t('Translation has been received for MotionPoint job id @id', ['@id' => $api_job_id]));
            $form['motionpoint_status'] = [
              '#markup' => t("Translation completed by MotionPoint."),
              '#weight' => -10,
            ];
          }
        }
        else {
          $form['motionpoint_status'] = [
            '#markup' => t("Job is in progress."),
            '#weight' => -10,
          ];          
        }
      }
    }

    if ($job->isActive() && $job->getCountPending() == 0 &&($job->getCountTranslated() > 0 || $job->getCountAccepted() > 0)) {
      $form['motionpoint_status'] = [
        '#markup' => t("Translation completed by MotionPoint."),
        '#weight' => -10,
      ];
    }

    if ($job->isFinished()) {
      $form['motionpoint_status'] = [
        '#markup' => t("This job is already finished."),
        '#weight' => -10,
      ];
    }

    return $form;
  }

  
  /**
   * Return available submit settings
   *
   * @return array
   */
  public function getSubmitSetting()
  {
    $out = [];
    $submitSettings = [
      0 => t('Single Job - all pages are combined into one MotionPoint job'),
      1 => t('Multiple Jobs - an individual MotionPoint job is created for each page'),
    ];
    foreach ($submitSettings as $value) {
        $out[] = $value;
    }
    return $out;
  }


  /**
   * Pull translations from MotionPoint.
   */
  public function submitPullTranslations(array $form, $form_state) {
    /** @var \TMGMTJob $job */
    $job = $form_state['tmgmt_job'];

    /** @var \TMGMTMotionpointTranslatorPluginController $controller */
    $controller = $job->getTranslatorController();
    $controller->pullTranslations($job);
  }
}
