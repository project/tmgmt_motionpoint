<?php

/**
 * @file
 * Provides MotionPoint translation plugin controller.
 */

/**
 * MotionPoint translation plugin controller.
 */
class TMGMTMotionpointTranslatorPluginController extends TMGMTDefaultTranslatorPluginController {
    
  /**
  * Constants
  */
  const JOB_CONTENT_TYPE = 'application/xliff+xml';
  const JOB_COMMENT = 'This job is from Drupal connector';
  const JOB_REFERENCE_ID = 'drupal job and item id';
  const MP_WORDS_TRANSLATED = 'words-translated=';
  const MP_WORDS_NOT_TRANSLATED = 'words-not-translated=';
  const MP_WORDS_SUPPRESSED = 'words-suppressed=';
  const MP_WORDS_NOT_QUEUEABLE = 'words-not-queueable=';
  const MP_JOB_STATUS_QUEUED = 'QUEUED';
  const MP_JOB_STATUS_COMPLETED = 'COMPLETED';
  const MP_SANDBOX_URL = "https://sandboxapi.motionpoint.com";
  const MP_SANDBOX_CLIENT_ID = 1;
  const MP_SANDBOX_SRC_LANG = 'en';
  const MP_SANDBOX_DESTINATION_LANG = 'es';
  const MULTIPART_FORM_BOUNDARY = '------------------------iBWEmnE4qy';
  const MP_CALLBACK_STATUS_COMPLETE = 'TRANSLATION_COMPLETED';

  
  /**
   * TMGMT translator.
   *
   * @var \Drupal\tmgmt\TranslatorInterface
   */
  protected $translator;
  
  
  /**
   * Sets a Translator.
   *
   * @param \Drupal\tmgmt\TranslatorInterface $translator
   */
  public function setTranslator(TMGMTTranslator $translator) {
    $this->translator = $translator;
  }


  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    $defaults = parent::defaultSettings();
    $defaults['export_format'] = 'xlf';
    $defaults['xliff_cdata'] = TRUE;
    return $defaults;
  }
  
  
  /**
   * Implements TMGMTTranslatorPluginControllerInterface::isAvailable().
   */
  public function isAvailable(TMGMTTranslator $translator) {
    if ($translator->getSetting('motionpoint_apikey')) {
      return TRUE;
    }
    return FALSE;
  }
  
  
  /**
   * Get the number of words will be translated
   *
   * @param TMGMTJob $job
   *
   * @return object|bool
   */
  public function getStatistics(TMGMTJob $job) {
    $this->setTranslator($job->getTranslator());
    
    $source_language = $this->translator->mapToRemoteLanguage($job->source_language);
    $target_language = $this->translator->mapToRemoteLanguage($job->target_language);
    $job_id = $job->tjid;
    
    $xliff_converter = tmgmt_file_format_controller('xlf');
    $xliff_content = $xliff_converter->export($job);
    $file_name = "mpdrupaljobid_{$job_id}_{$source_language}_{$target_language}.xlf";
    
    $options = "";
    $options .= "--".STATIC::MULTIPART_FORM_BOUNDARY."\r\n";
    $options .= "Content-Disposition: form-data; name=\"file\"; filename=\"$file_name\"\r\nContent-Type: text/plain\r\n\r\n";
    $options .= $xliff_content;
    $options .= "\r\n\r\n";
    $options .= $this->addMultipartFormContentDispositionElement('transactionReferenceId', STATIC::JOB_REFERENCE_ID . ' ' . $job_id, STATIC::MULTIPART_FORM_BOUNDARY);
    $options .= $this->addMultipartFormContentDispositionElement('comments', STATIC::JOB_COMMENT, STATIC::MULTIPART_FORM_BOUNDARY);
    $options .= $this->addMultipartFormContentDispositionElement('contentType', STATIC::JOB_CONTENT_TYPE, STATIC::MULTIPART_FORM_BOUNDARY);
    $options .= $this->addMultipartFormContentDispositionElement('contentCharset', 'utf-8', STATIC::MULTIPART_FORM_BOUNDARY);
    $options .= $this->addMultipartFormContentDispositionElement('queueUntranslatedContent', 'false', STATIC::MULTIPART_FORM_BOUNDARY);
    $options .= "--".STATIC::MULTIPART_FORM_BOUNDARY."--";
 
    $response = $this->sendRequest('/translations', 'POST', $options, $source_language, $target_language);

    if (isset($response->code) && $response->code == 200) {

      $mpstats = $response->headers['x-mptrans-statistics'];

      $start_translated = strpos($mpstats, static::MP_WORDS_TRANSLATED) + strlen(static::MP_WORDS_TRANSLATED);
      $end_translated = strpos($mpstats, static::MP_WORDS_NOT_TRANSLATED) - $start_translated - 1;
      $words_translated = substr($mpstats, $start_translated, $end_translated);
      
      $start_not_translated = strpos($mpstats, static::MP_WORDS_NOT_TRANSLATED) + strlen(static::MP_WORDS_NOT_TRANSLATED);
      $end_not_translated = strpos($mpstats, static::MP_WORDS_SUPPRESSED) - $start_not_translated - 1;
      $words_not_translated = substr($mpstats, $start_not_translated, $end_not_translated);

      $start_suppressed = strpos($mpstats, static::MP_WORDS_SUPPRESSED) + strlen(static::MP_WORDS_SUPPRESSED);
      $end_suppressed = strpos($mpstats, static::MP_WORDS_NOT_QUEUEABLE) - $start_suppressed - 1;
      $words_suppressed = substr($mpstats, $start_suppressed, $end_suppressed);
      
      $words_total = $words_translated + $words_not_translated + $words_suppressed; 

      $quote['words_not_translated'] = $words_not_translated;
      $quote['words_total'] = $words_total;
      return $quote;
    }
    elseif (isset($response->code) && $response->code == 404) {
      $quote['language_not_support'] = TRUE;
      return $quote;
    }
    else {
      return FALSE;
    }
  }
  
  
  /**
   * Implements TMGMTTranslatorPluginControllerInterface::requestTranslation().
   */
  public function requestTranslation(TMGMTJob $job) {
    $mode_from_translator = $job->getTranslator()->getSetting('submit_setting');
    $mode_from_job = $job->getSetting('submit_setting' . strval($mode_from_translator));
    
    if (!is_null($mode_from_job) && $mode_from_job == 1) {
      $this->requestJobItemsTranslation($job->getItems());
      return;
    }

    $this->setTranslator($job->getTranslator());
    $source_language = $this->translator->mapToRemoteLanguage($job->source_language);
    $target_language = $this->translator->mapToRemoteLanguage($job->target_language);
    $job_id = $job->tjid;

    $xliff_converter = tmgmt_file_format_controller('xlf');
    $xliff_content = $xliff_converter->export($job);
    
    $callbackurl = url("tmgmt_motionpoint/callback/job/$job_id", ['absolute' => TRUE]);
    
    $api_job_id = $this->createApiJob($job_id, $source_language, $target_language, $xliff_content, $callbackurl);
    if ($api_job_id) {
      $job->submitted(
          'Created a new job in MotionPoint @source->@target with the reference id: @id',
          [
            '@id' => $api_job_id, 
            '@source' => strtoupper($source_language), 
            '@target' => strtoupper($target_language)
          ], 
          'status'
      );
      if ((int) $job->reference < 1) {  // job reference has not been set before
        $job->reference = $api_job_id; 
        $job->save();
      }
    }
  }
  
  
  /**
   * Get translation job status
   *
   * @param int $id
   * @param string $source_language
   * @param string $target_language
   * 
   * @return string
   * Return the api job status
   */
  public function getApiJobStatus($id, $source_language, $target_language) {
    $api_job_status = 'UNKNOWN';
    $path = '/translationjobs' . '/' . $id;
    $response = $this->sendRequest($path, 'POST', [], $source_language, $target_language);
    
    if (isset($response->code) && $response->code == 200) {
      $api_job_status = json_decode($response->data)->{'status'};
      return $api_job_status; 
    }
    
    return $api_job_status;
  }
  
  
  /**
   * Get translations for API job of a given drupal job.
   *
   * @param int $id
   * @param string $source_language
   * @param string $target_language
   *  
   * @return array
   * Translated data array.
   */
  public function getTranslations($id, $source_language, $target_language) {
    $path = '/translations/jobs' . '/' . $id;
    $response = $this->sendRequest($path, 'POST', [], $source_language, $target_language);
    $received_translation = [];

    if (isset($response->code) && $response->code == 200) {
      $data = $response->data; //  get response body as string then format as xliff 

      $xliff_converter = tmgmt_file_format_controller('xlf');
      $received_translation = $xliff_converter->import($data, FALSE);
    }
    
    return $received_translation;
  }
  
  
  /**
   * Fetch translation and update job after received translation.
   * 
   * @return bool
   * Returns TRUE if success, otherwise FALSE
   */
  public function updateTranslationsToJob(TMGMTJob $job, $api_job_id, $jobitemid = NULL) {
    $source_language = $this->translator->mapToRemoteLanguage($job->source_language);
    $target_language = $this->translator->mapToRemoteLanguage($job->target_language);

    try {
      $received_translation = $this->getTranslations($api_job_id, $source_language, $target_language);
      
      $job_item_id_received = NULL;
      if (!empty($received_translation)) {
        $job_item_id_received = array_keys($received_translation)[0];
      }
      
      if (isset($jobitemid) && $job_item_id_received != $jobitemid) {
        return FALSE;
      }
      // Xliff that could not be parsed will return a 
      // boolean and not show a proper error
      if (is_bool($received_translation) === TRUE) {
        throw new Exception(); 
      }
      $job->addTranslatedData(
          $received_translation, 
          [], 
          TMGMT_DATA_ITEM_STATE_TRANSLATED
      );
      $job->addMessage(
          'Translation has been received for MotionPoint job id @id', 
          [
            '@id' => $api_job_id
          ]
      );
      $this->approveApiJob($api_job_id, $source_language, $target_language);
      return TRUE;
    }
    catch (Exception $e) {
      watchdog(
          'tmgmt_motionpoint', 
          'Translation cannot be retrieved. If situation persists please contact support@motionpoint.com.', 
          [],
          WATCHDOG_WARNING
      );
    }
    return FALSE;
  }

  
  /**
   * Implements TMGMTTranslatorPluginControllerInterface::getSupportedLanguages().
   */
  public function getSupportedRemoteLanguages(TMGMTTranslator $translator) {
    $remote_languages = [];
    return $remote_languages;
  }
  
  /**
   * Create API job when user requests translation
   *
   * @param int $id
   * @param string $source_language
   * @param string $target_language
   *  
   * @return int|bool
   *  Returns API job id if job is created, otherwise FALSE
   */
  public function createApiJob($id, $source_language, $target_language, $xliff_content, $callbackurl) {
    $file_name = "mpdrupaljobid_{$id}_{$source_language}_{$target_language}.xlf";
    
    // building multipart form
    $options = "";
    $options .= "--".STATIC::MULTIPART_FORM_BOUNDARY."\r\n";
    $options .= "Content-Disposition: form-data; name=\"file\"; filename=\"$file_name\"\r\nContent-Type: text/plain\r\n\r\n";
    $options .= $xliff_content;
    $options .= "\r\n\r\n";
    $options .= $this->addMultipartFormContentDispositionElement('transactionReferenceId', static::JOB_REFERENCE_ID . ' ' . $id, STATIC::MULTIPART_FORM_BOUNDARY);
    $options .= $this->addMultipartFormContentDispositionElement('comments', static::JOB_COMMENT, STATIC::MULTIPART_FORM_BOUNDARY);
    $options .= $this->addMultipartFormContentDispositionElement('contentType', "application/x-xliff+xml", STATIC::MULTIPART_FORM_BOUNDARY);
    $options .= $this->addMultipartFormContentDispositionElement('contentCharset', "utf-8", STATIC::MULTIPART_FORM_BOUNDARY);
    $options .= $this->addMultipartFormContentDispositionElement('callbackUrl', $callbackurl, STATIC::MULTIPART_FORM_BOUNDARY);
    $options .= "--".STATIC::MULTIPART_FORM_BOUNDARY."--";

    $response = $this->sendRequest('/translationjobs', 'POST', $options, $source_language, $target_language); 
    if (isset($response->code) && $response->code == 200) {
      $data = json_decode($response->data);
      return $data->{'id'};
    }
    else {
      drupal_set_message(t('Unable to send job to MotionPoint.'), 'warning');
      return FALSE;
    }
  }
  
  
  /**
   * Set API job to approved
   *
   * @param int $id
   * @param string $source_language
   * @param string $target_language
   *  
   */
  public function approveApiJob($id, $source_language, $target_language) {
    $path = '/translationjobs/status';  
    $payload = array(
      'jobId' => $id,
      'newStatusId' => '7',
    );
    $options['data'] = json_encode($payload);
    $this->sendRequest($path, 'POST', $options, $source_language, $target_language);
  }
  
  
  /**
   * {@inheritdoc}
   */
  public function abortTranslation(TMGMTJob $job) {
    $this->setTranslator($job->getTranslator());

    $source_language = $this->translator->mapToRemoteLanguage($job->source_language);
    $target_language = $this->translator->mapToRemoteLanguage($job->target_language);
    $job_reference = (string)$job->reference;
    $api_job_id = explode("-", $job_reference, 2)[0];

    $mode_from_translator = $job->getTranslator()->getSetting('submit_setting');
    $mode_from_job = $job->getSetting('submit_setting' . strval($mode_from_translator));

    if (!is_null($mode_from_job) && $mode_from_job == 1) {

        $count_items = count($job->getRemoteMappings());
        $count_cancelled = 0;
        $cancelled_ids = '';

        foreach ($job->getRemoteMappings() as $remote) {
          $job_item = $remote->getJobItem();
          $api_job_id = $remote->remote_identifier_2;
          if ($job_item->isActive()) {
            $cancel_result = $this->cancelApiJob($api_job_id, $source_language, $target_language);
            if ($cancel_result) {
              $job_item->setState(TMGMT_JOB_ITEM_STATE_ABORTED);
              $cancelled_ids = $cancelled_ids . ' ' . $api_job_id;
              $count_cancelled++;
            }
          }
          elseif ($job_item->isAborted()) {
              $count_cancelled++;
          }
        }
      
        if ($count_cancelled == 0 || $cancelled_ids == '') {
          drupal_set_message(
              t('Cannot abort this job. All items in this job are being processed or have been aborted already.'),
              'warning'
          );
        }
        else {
          if ($count_cancelled == $count_items) {
            $job->aborted('All job items have been aborted.');
          }
          else {
            $job->addMessage(
              'Following MotionPoint job ids have been aborted: @cancelled_ids. Others are being processed or have been aborted already.',
                [
                  '@cancelled_ids' => $cancelled_ids
                ]
            );
          }
        }
    }
    else {
      if (strpos($job_reference, static::MP_JOB_STATUS_COMPLETED) !== FALSE) {
        drupal_set_message(
            t('Translation is being processed. This job can no longer be aborted.'), 
            'warning'
        );
      }
      else {
        $cancel_result = $this->cancelApiJob($api_job_id, $source_language, $target_language);
        if ($cancel_result) {
          $job->aborted(
              'This job has been aborted. Motionpoint will not process the content requested in this job.'
          );
        }
        else {
          drupal_set_message(
              t('Translation is being processed. This job can no longer be aborted.'),
              'warning'
          );
        }
      }
    }
  }

  
  /**
   * Cancel API job when user aborts job
   *
   * @param int $id
   * @param string $source_language
   * @param string $target_language
   *  
   * @return bool
   *   TRUE if the job is cancelled, FALSE otherwise.
   */
  public function cancelApiJob($id, $source_language, $target_language) {
    $path = '/translationjobs/status';  
    $payload = array(
      'jobId' => $id,
      'newStatusId' => '6',
    );
    $options['data'] = json_encode($payload);
    $response = $this->sendRequest(
        $path, 
        'POST', 
        $options, 
        $source_language, 
        $target_language
    );
    
    if (isset($response->code) && $response->code == 200) {
      return TRUE;
    }
    elseif (isset($response->code) && $response->code == 409) {
      return FALSE;
    }
  }
  
  
  /**
   * Does a request to MotionPoint API.
   *
   * @param string $path
   *   Resource path.
   * @param string $method
   *   (Optional) HTTP method (GET, POST...). By default uses GET method.
   * @param array $params
   *   (Optional) Additional Form parameters to send.
   * @param string $source_language
   *   (Optional) Source language when request translation.
   * @param array $target_language
   *   (Optional) Target language when request translation.
   * @return object
   *   Response object
   *
   * @throws \Drupal\tmgmt\TMGMTException
   */
  public function sendRequest($path, $method = 'GET', $params = [], $source_language = NULL, $target_language = NULL) {
    if (!$this->translator) {
      throw new TMGMTException('There is no Translator entity. Unable to access to the MotionPoint API.');
    }
    
    $sandbox = !is_null($sandbox = $this->translator->getSetting('use_sandbox')) 
              ? $sandbox
              : 0;
    
    if ($sandbox == 1) {
      $url = STATIC::MP_SANDBOX_URL . $path;
      $username = $this->translator->getSetting('motionpoint_username');
      $apikey = $this->translator->getSetting('motionpoint_apikey');
      $client_id = STATIC::MP_SANDBOX_CLIENT_ID;
      $source_language = STATIC::MP_SANDBOX_SRC_LANG;
      $target_language = STATIC::MP_SANDBOX_DESTINATION_LANG;
    }
    else {
      $url = $this->translator->getSetting('motionpoint_api_url') . $path;
      $username = $this->translator->getSetting('motionpoint_username');
      $apikey = $this->translator->getSetting('motionpoint_apikey');
      $client_id = $this->translator->getSetting('motionpoint_client_id');
    }

    $options = [];
    $options['method'] = $method;

    if ($path == '/queues') {
      $options['headers'] = [
        'Authorization' => $apikey,
        'X-MotionCore-UserName' => $username,
      ];
      
    }
    elseif ($path == '/translationjobs' || $path == '/translations') {
      $options['headers'] = [
        'Authorization' => $apikey,
        'X-MotionCore-UserName' => $username,
        'X-MotionCore-Queue' => $source_language . '.' . $target_language . '.' . $client_id,
      ];
      
      $headers = ['Content-Type' => "multipart/form-data; boundary=" . STATIC::MULTIPART_FORM_BOUNDARY];
      $options['headers'] = array_merge($options['headers'], $headers);
      $options['data'] = $params;
      
    }
    else {
      $options['headers'] = [
        'Authorization' => $apikey,
        'X-MotionCore-UserName' => $username,
        'X-MotionCore-Queue' => $source_language . '.' . $target_language . '.' . $client_id,
        'Content-Type' => 'application/json',
      ];
      
      $options += $params;
    }

    try {
      $response = drupal_http_request($url, $options);
      return $response;
    }
    catch (Exception $e) {
      watchdog('tmgmt_motionpoint', $e->getMessage(), array(), WATCHDOG_ERROR);
    }
  }

  
  public function requestJobItemsTranslation(array $job_items) {
    $job = reset($job_items)->getJob();
    $this->setTranslator($job->getTranslator());
    $source_language = $this->translator->mapToRemoteLanguage($job->source_language);
    $target_language = $this->translator->mapToRemoteLanguage($job->target_language);
    $job_id = $job->tjid;
    $api_job_ids = '';

    foreach ($job_items as $job_item) {

        $job_item_id = $job_item->tjiid;
        $unique_job_id = $job_id . '-' .$job_item_id;
        $xliff_content = $this->getXliffData($job, [$job_item]);
        $callbackurl = url("tmgmt_motionpoint/callback/jobitem/$job_item_id", ['absolute' => TRUE]);
        
        $api_job_id = $this->createApiJob($unique_job_id, $source_language, $target_language, $xliff_content, $callbackurl);
        if ($api_job_id) {
          $job_item->addRemoteMapping(
              NULL, 
              $unique_job_id, 
              [
                'remote_identifier_2' => $api_job_id
              ]
          );
          $api_job_ids = $api_job_ids . ' ' . $api_job_id;
        }
        else {
          return;
        }
    }
     
    $job->submitted(
      'Created new jobs in MotionPoint @source->@target with the reference ids: @id', 
      [
        '@id' => $api_job_ids,
        '@source' => strtoupper($source_language),
        '@target' => strtoupper($target_language)
      ],
      'status'
    );
    $job->reference = 'MULTI';
    $job->save();
  }
  
  
  /**
  * Returns the XLIFF data for a JobInterface based on job items given.
  *
  * @param \Drupal\tmgmt\JobInterface $job
  *   The translation job.
  * @param \Drupal\tmgmt\JobItemInterface[] $job_items
  *   Limit the export to the provided job items.
  *
  * @return array
  */
  public function getXliffData(TMGMTJob $job, array $job_items) {
    $job_item_ids = [];
    // Only one item in the array so only a portion of the XLIFF content is taken
    foreach ($job_items as $job_item) {
      $job_item_ids[$job_item->tjiid] = $job_item;
    }
    
    $conditions['tjiid'] = ['value' => array_keys($job_item_ids), 'operator' => 'IN'];

    $xliff_converter = tmgmt_file_format_controller('xlf');
    $data = $xliff_converter->export($job, $conditions);
   
    return $data;
  }
  
  
  /**
   * Adds a content disposition element to a multipart form.
   *
   * @param string $name
   *   Name of the element.
   * @param string $value
   *   Value of the element.
   * @param string $boundary
   *   Boundary meant to partition the elements.
   *
   * @return string
   *   A content disposition element for a mutlipart form.
   */
  protected function addMultipartFormContentDispositionElement($name, $value, $boundary) {
    return "--$boundary\r\nContent-Disposition: form-data; name=\"$name\"\r\n\r\n$value\r\n";
  }
  
  
  public function jobCallback(TMGMTJob $tmgmt_job, $request) {
    $response_body = 'Request body is empty.';

    if (!(empty($request))) {
      $api_job_id = $request['jobId'];
      $api_job_status = $request['eventType'];
      
      if ($tmgmt_job->isActive() 
          && $api_job_status == static::MP_CALLBACK_STATUS_COMPLETE
      ) {
          $translator_plugin = $tmgmt_job->getTranslator()->getController();
          $translator_plugin->setTranslator($tmgmt_job->getTranslator());
          $result = $translator_plugin->updateTranslationsToJob($tmgmt_job, $api_job_id);
          if ($result) {
            $response_body = 'Translation has been successfully updated to drupal job.';
          }
          else {
            $response_body = 'Failed to update translation to drupal job.';
            drupal_add_http_header('Status', '500');
            echo $response_body;
            return;
          }
      }
      elseif (!$tmgmt_job->isActive()) {
        $response_body = 'Drupal job is no longer active.';
      }
      elseif ($api_job_status != static::MP_CALLBACK_STATUS_COMPLETE) {
        $response_body = 'Api job status is not COMPLETED yet.';
      }
    }

    drupal_add_http_header('Status', '200 OK');
    echo $response_body;
  }
  
  
  public function jobItemCallback(TMGMTJobItem $tmgmt_job_item, $request) {
    $response_body = 'Request body is empty.';
    if (!(empty($request))) {
      $api_job_id = $request['jobId'];
      $api_job_status = $request['eventType'];
        
      if ($tmgmt_job_item->isActive() 
          && $api_job_status == static::MP_CALLBACK_STATUS_COMPLETE
      ) {
          $job = $tmgmt_job_item->getJob();
          $translator_plugin = $job->getTranslator()->getController();
          $translator_plugin->setTranslator($job->getTranslator());
          $result = $translator_plugin->updateTranslationsToJob($job, $api_job_id);
          if ($result) {
            $response_body = 'Translation has been successfully updated to drupal job item.';
          }
          else {
            $response_body = 'Failed to update translation to drupal job item.';
            drupal_add_http_header('Status', '500');
            echo $response_body;
            return;
          }
        }
        elseif (!$tmgmt_job_item->isActive()) {
          $response_body = 'Drupal job item is no longer active.';
        }
        elseif ($api_job_status != static::MP_CALLBACK_STATUS_COMPLETE) {
          $response_body = 'Api job status is not COMPLETED yet.';
        }
    }

    drupal_add_http_header('Status', '200 OK');
    echo $response_body;
  }


   /**
   * Pulls translations for job items of a given job.
   *
   * @param TMGMTJob $job
   */
  public function pullTranslations(TMGMTJob $job) {
    $this->setTranslator($job->getTranslator());
    $source_language = $this->translator->mapToRemoteLanguage($job->source_language);
    $target_language = $this->translator->mapToRemoteLanguage($job->target_language);

    $pulled_translation = 0;

    /** @var \TMGMTRemote[] $remotes */
    foreach ($job->getRemoteMappings() as $remote) {

      $job_item = $remote->getJobItem();
      $api_job_id = $remote->remote_identifier_2;

      if ($job_item->isActive() && $job_item->getCountPending() > 0) {
        $api_job_status = $this->getApiJobStatus($api_job_id, $source_language, $target_language);
        if ($api_job_status == static::MP_JOB_STATUS_COMPLETED) {
          $result = $this->updateTranslationsToJob($job, $api_job_id, $job_item->tjiid);
          if ($result) {
            $pulled_translation++;
          }
        }
      }
    }

    if ($pulled_translation > 0) {
      drupal_set_message(t('Translation has been received for some items.'));
    }
    else {
      drupal_set_message(t('No new translation has been received.'), 'warning');
    }
  }
}
