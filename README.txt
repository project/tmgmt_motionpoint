CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------

TMGMT MotionPoint module is a plugin for Translation Management Tool module (tmgmt).
With the MotionPoint Translation Provider, TMGMT editors can submit their Drupal
content directly to MotionPoint translation system. There the content will be translated and sent back
to the Drupal website for review and acceptance. After accepting the translation
it will be published automatically.MotionPoint Plugin provide continous translation.


Requirements
------------
This module requires MotionPoint API Key and API ID You can obtain
them by contacting support@motionpoint.com


Recommended modules
----------------
This module requires TMGMT(http://drupal.org/project/tmgmt) module for CMS 7
to be installed. 

Install and Activate the following Drupal modules for 7

Entity API
Views
Chaos Tools (Base for Views)
Views Bulk Operations
Content Translation (for the content translation source)
Locale
Internationalization/i18n (for string translation)
Entity translation (for the entity source)
Rules (for node translation)

Installation
-------------

Please refer to the Installation Guide attached to the Project.

https://www.drupal.org/project/tmgmt_motionpoint

Configuration
----------------

Key in the API Key and API ID in the MotionPoint Translation Provider Configuration UI.
Click Connect to Authenticate with MotiontPoint Backend System. If connection is successful Editors can submit
translated content to MotionPoint.


Troubleshooting
----------------

For TroubleShooting copy the Error Message and Email to support@motionpoint.com

Maintainers
----------------

Sugumaran Thangaswamy - https://www.drupal.org/u/sugumarant
Yunpeng Li - https://www.drupal.org/u/yli


